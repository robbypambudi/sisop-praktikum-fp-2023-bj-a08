# Menggunakan base image yang sesuai dengan kebutuhan Anda
FROM gcc:latest

# Menentukan working directory di dalam container
WORKDIR /app

COPY /database/database.c ./database/

RUN gcc ./database/database.c -o ./database/database
RUN chmod +x ./database/database
ENTRYPOINT [ "./database/database" ]
EXPOSE 8080
