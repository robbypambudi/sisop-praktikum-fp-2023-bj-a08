#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080


char rpath[1024] = "../databases";

int check_user_pass(char *user, char *pass)
{
    char path[1024];
    strcpy(path, rpath);
    strcat(path, "/users/user_list"); 

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printf("Failed to open user_list.txt\n");
        return 0;
    }

    char line[1024];
    int found = 0;

    while (fgets(line, sizeof(line), file))
    {
        // Remove newline character from the line
        size_t len = strlen(line);
        if (len > 0 && line[len - 1] == '\n')
        {
            line[len - 1] = '\0';
        }

        char *token = strtok(line, "-");
        // printf("user : %s", token);

        if (strcmp(token, user) == 0)
        {
            token = strtok(NULL, "-");

            // Remove newline character from the token
            len = strlen(token);
            if (len > 0 && token[len - 1] == '\n')
            {
                token[len - 1] = '\0';
            }

            // printf(" pass : %s || token : %s\n", token, pass);

            if (strcmp(token, pass) == 0) {
                found = 2;
            } else {
                found = 1;
            }
            break;
        }
        else {
            puts("");
        }
    }

    fclose(file);

    return found;
}


  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    // Check sudo
    uid_t uid=getuid(), euid=geteuid();
    int sudo;
    if(geteuid() != 0)
    {
        char unsudo[] = "unsudo";
        send(sock , unsudo, strlen(unsudo), 0 );
        sudo = 0;
    }
    else 
    {
        char sudo_msg[] = "sudo";
        send(sock , sudo_msg, strlen(sudo_msg), 0 );
        sudo = 1;
    }

    // get uname and pw
    char username[60]; 
    char password[60];
    char msg[1024];
    int success = 0;
    int checker = 0;

    if(sudo == 0)
    {
        if(argv[1] != NULL)
        {
            if(argv[2] != NULL && strcmp(argv[1], "-u") == 0)
            {
                if(argv[3] != NULL && strcmp(argv[3], "-p") == 0)
                {
                    if(argv[4] != NULL)
                    {
                        strcpy(username, argv[2]);
                        strcpy(password, argv[4]);
                        checker = check_user_pass(username, password);

                        if(checker == 1)
                        {
                            printf("%s\n", password);
                            printf("Wrong password.\n");
                        }
                        else if(checker == 2)
                        {
                            success = 1;
                            printf("successfully logged in.\n");
                        }
                        else if (checker == 0) printf("Username not found.\n");
                    }
                }
            }
        }

        if (success == 0) {
            strcpy(msg, "sorry, failed to log in");
            send(sock, msg, strlen(msg), 0);
            exit(1);
        }
        else send(sock, username, strlen(username), 0);
    }

    printf("\n--------Hello from client side---------\n");

    while(1){
        char err_msg[1024] = {0};
        char input[100];
        memset(input, 0, sizeof input);
        scanf("%[^\n]%*c", input);
        send(sock, input, strlen(input), 0);
        if(strcmp(input, "QUIT") == 0 || strcmp(input, "quit") == 0) {
            break;
            return -1;
        }
        read(sock, err_msg, 1024);
        printf("%s\n", err_msg);
    }
}
