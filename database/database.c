#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#define PORT 8080

char rpath[1024] = "../databases";

int generate_thead(char *output, char data[100][100], char type[100][100], int total_data)
{
    int left_space;
    char temp[60];
    char table[1024] = "";

    strcpy(output, "|");

    for (int i = 0; i < total_data; i++)
    {
        memset(temp, 0, 60);
        sprintf(temp, "%s (%s)", data[i], type[i]);

        if(strcmp(type[i], "string") == 0)
        {
            left_space = 50 - strlen(temp);
            if (left_space < 0) return 11;
        }
        else if(strcmp(type[i], "char") == 0)  
        {
            left_space = 20 - strlen(temp);
            if (left_space < 0) return 12;
        }
        else if(strcmp(type[i], "int") == 0)  
        {
            left_space = 20 - strlen(temp);
            if (left_space < 0) return 13;

        }
        else return 14;

        char space[60] = "";
        for(int i = 0; i < left_space; i++)
        {
            strcat(space, " ");
        }

        sprintf(table, " %s%s |", temp, space);

        strcat(output, table);
    }
    return 1;
}
 
int generate_table(char *table_path, char column_name[100][100], char type[100][100], int total_col)
{
    int flag;
    FILE *table = fopen(table_path, "w");

    char printed_row[1024];
    flag = generate_thead(printed_row, column_name, type, total_col);

    if(flag != 1) {
        fclose(table);
        return flag;
    }

    int length = strlen(printed_row);
    char text[1024 * 4];
    char border[1024] = "";

    for(int i = 0; i < length; i++){
        strcat(border, "-");
    }

    sprintf(text, "%s\n%s\n%s\n", border, printed_row, border);
    fputs(text, table);

    fclose(table);

    return 1;
}

int generate_record(char *table_path, char column_name[100][100], char type[100][100], int total_col)
{
    // open table
    FILE *table = fopen(table_path, "w");

    if(table == NULL) {
        return 0;
    }

    char printed_row[1024] = "";
    for (int i = 0; i < total_col; i++) {
        strcat(printed_row, column_name[i]);
        if (i < total_col - 1) {
            strcat(printed_row, "-");
        }
    }

    fputs(printed_row, table);
    fputs("\n", table);
    fclose(table);

    return 1;
}

int insert_row(char *table_path, char data[100][60])
{
    FILE *table;
    char err_msg[1024] = {0};
    char readLine[2084] = {0};
    char printedRow[1024];
    char container[70] = "";
    char *savePointer;
    int counter = 0;
    int dataCounter = 0;
    int length;
    
    if (table = fopen(table_path, "r")) // file exists
    {
        // Initialization
        strcpy(printedRow, "|");

        // Counting width
        while (fgets(readLine, sizeof(readLine), table)) 
        { 
            if (counter < 1) {
                counter++;
                continue;
            }
            char *token = strtok_r(readLine, "(", &savePointer);
            token = strtok_r(NULL, "(", &savePointer); 
            while (token != NULL)
            {
                if (token[0] == 's') // string
                {
                    length = 50;
                    if (strlen(data[dataCounter]) > length) 
                    {
                        fclose(table);
                        return 11;
                    }
                    
                    if(data[dataCounter][0] != '\'' || data[dataCounter][strlen(data[dataCounter]) - 1] != '\'') // alfabet tanpa tanda petik
                    {
                        fclose(table);
                        return 16;
                    }
                }
                else if (token[0] == 'c') // char
                {
                    length = 20;
                    if (strlen(data[dataCounter]) > length)
                    {
                        fclose(table);
                        return 12;
                    }

                    if(data[dataCounter][0] != '\'' || data[dataCounter][strlen(data[dataCounter]) - 1] != '\'') // alfabet tanpa tanda petik
                    {
                        fclose(table);
                        return 15;
                    }
                } 
                else if (token[0] == 'i') // int
                {
                    length = 20;
                    if (strlen(data[dataCounter]) > length)
                    {
                        fclose(table);
                        return 13;
                    }
                    for (int i = 0; i < strlen(data[dataCounter]); i ++)
                    {
                        if (data[dataCounter][i] < 48 || data[dataCounter][i] > 57) 
                        {
                            fclose(table);
                            return 14;
                        }
                    }
                }

                length -= strlen(data[dataCounter]);

                char space[60] = "";
                for(int k = 0; k < length; k++)
                {
                    strcat(space, " ");
                }

                sprintf(container, " %s%s |", data[dataCounter], space);
                strcat(printedRow, container);
                dataCounter++;
                token = strtok_r(NULL, "(", &savePointer); 
            }
            break;
        }

        // write row ke file
        fclose(table);
        table = fopen(table_path, "a");
        strcat(printedRow, "\n");
        fputs(printedRow, table);
        printf("%s\n", printedRow);
        fclose(table);
        return 1;
    }
    else // file does not exist
    {
        return 0;
    }

    return 0;
}

int insert_row_user(char *table_path, char data[100][60])
{
    FILE *table;
    char printedRow[1024];
    
    table = fopen(table_path, "a");
    if (table == NULL) {
        printf("di insert row\n");
        printf("Failed to open user_list.txt\n");
        return 0;
    }
    
    sprintf(printedRow, "%s-%s\n", data[0], data[1]);
    fputs(printedRow, table);
    fclose(table);
    
    return 1;
}

int check_user(char *user)
{
    char path[1024];
    strcpy(path, rpath);
    strcat(path, "/users/user_list");
    
    FILE *table = fopen(path, "r");
    if (table == NULL) {
        printf("di check user\n");
        printf("Failed to open user_list.txt\n");
        return 0;
    }

    char line[1024];
    int found = 0;

    while (fgets(line, sizeof(line), table)) 
    {
        line[strcspn(line, "\n")] = '\0'; // Remove trailing newline

        if (strcmp(line, user) == 0)
        {
            found = 1;
            break;
        }
    }

    fclose(table);

    if (found) return 1;

    return 0;
}

int create_user(char *token)
{
    char path[1024];
    strcpy(path, rpath);
    strcat(path, "/users/user_list");
    printf("%s\n", path);

    char username[100], password[100];

    token = strtok(NULL, " ");
    if (token == NULL) return 0;

    int user_exist = check_user(token);

    if (user_exist == 0)
    {
        strcpy(username, token);

        token = strtok(NULL, " \n");

        if (token == NULL || strcmp(token, "IDENTIFIED") != 0) return 0;
        
        token = strtok(NULL, " \n");
        if (token == NULL || strcmp(token, "BY") != 0) return 0;

        token = strtok(NULL, " \n");
        if (token == NULL) return 0;
        else
        {
            strcpy(password, token);

            char data[100][60];
            strcpy(data[0], username);
            strcpy(data[1], password);

            int response = insert_row_user(path, data);

            if (response == 0)
            {
                return 2;
            }
            else
            {
                return response;
            }
        }
    }
    else if (user_exist == 1)
    {
        return 3;
    }

    return 0;
}
int check_database_user(char *database, char *user)
{
    char path[1024];
    strcpy(path, rpath);
    strcat(path, "/users/database_access");
    
    FILE *table = fopen(path, "r");
    char line[2048]; 
    char *saveptr_user;
    int found = 0, counter = 0, tok_counter = 0;

    if (check_user(user) == 0) return 3;

    while (fgets(line, sizeof(line), table)) 
    {
        if (counter < 3)
        {
            counter++;
            continue;
        }

        char *token = strtok_r(line, " ", &saveptr_user);
        token = strtok_r(NULL, " ", &saveptr_user); 

        if (strcmp(token, user) == 0)
        {
            token = strtok_r(NULL, " ", &saveptr_user); 
            token = strtok_r(NULL, " ", &saveptr_user); 
            if (strcmp(token, database) == 0)
            {
                found = 1;
                break;
            }
        }
    }

    fclose(table);

    return found;
}

void database_setup()
{
    char path[1024];
    char col[100][100] = {0};
    char type[100][100] = {0};
    int col_counter;
    char err_msg[1024] = {0};

    DIR* dir = opendir(rpath);
    if (dir) 
        closedir(dir);
    else if (ENOENT == errno) 
    {
        int flag;
        mkdir(rpath, 0777);
        
        // Creating database users
        strcpy(path, rpath);
        strcat(path, "/users");
        mkdir(path, 0777);

        // Creating table 'user_list'
        strcat(path, "/user_list");
        // Initialization
        memset(col, 0, sizeof col);
        col_counter = 2;
        strcpy(type[0], "string");
        strcpy(type[1], "string");

        // Inserting column names
        strcpy(col[0], "Username");
        strcpy(col[1], "Password");
        // Creating the table
        flag = generate_record(path, col, type, col_counter);
        if(flag)

        // Creating table 'database_access'
        strcpy(path, rpath);
        strcat(path, "/users");
        strcat(path, "/database_access");

        memset(col, 0, sizeof col);
        strcpy(col[0], "Username");
        strcpy(col[1], "Database");
        flag = generate_record(path, col, type, col_counter);

        // Creating history query directory
        stpcpy(path, rpath);
        strcat(path, "/history");
        mkdir(path, 0777);
        printf("created databases directory, history, users\n");
    } 
}

int create_database(char *token)
{
    char path[1024];
    int check;

    if (token == NULL) return 0;
    
    strcpy(path, rpath);
    strcat(path, "/");
    strcat(path, token);

    printf("%s\n", path);

    check = mkdir(path, 0777);

    if (check == -1){
        printf("Error Creating Database\n");
    }
    return check;
}


int check_dbaccess(char *database, char *user)
{
    // Check if the database directory exists
    char db_path[1024];
    strcpy(db_path, "../databases/");
    strcat(db_path, database);

    DIR *dir = opendir(db_path);
    if (dir == NULL) {
        printf("Database '%s' does not exist.\n", database);
        return 0;
    }
    closedir(dir);

    // check if the user exists
    int isUser = check_user(user);
    if(isUser == 0) {
        printf("User not found\n");
        return 3;
    };
    
    // Proceed with access check
    char access_path[1024];
    strcpy(access_path, rpath);
    strcat(access_path, "/users/database_access");

    FILE *file = fopen(access_path, "r");
    if (file == NULL) {
        printf("Failed to open database_access.txt\n");
        return -1;
    }

    char line[1024];
    int found = 0;

    while (fgets(line, sizeof(line), file))
    {
        char *token = strtok(line, "-");
        if (strcmp(token, user) == 0)
        {
            token = strtok(NULL, "-");
            if (strcmp(token, database) == 0) {
                found = 1;
                break;
            }
        }
    }

    fclose(file);

    if (found == 1)
        return 1;  // User and database match
    else
        return 2;  // User or database doesn't match
}


int find_table(char *path)
{
    FILE *table = fopen(path, "r");
    if (table != NULL )
    {
        fclose(table);

        return 51;
    }
    else if (ENOENT == errno) return 50;
    return 50;
}

int drop_column (char *table_path, char *table_name, char *col_name)
{
    char line[2048];
    char *savePointer;
    char columns[100][100];
    char data_type[100][100];
    char data[1024][100][60];
    int total_col = 0;
    int col_loc = -1;
    FILE *table = fopen(table_path, "r");

    // Searching for column
    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    char *token = strtok_r(line, " ", &savePointer);
    int skip = 0;
    while(token != NULL)
    {
        //printf("token = %c\n", token[0]);
        if(token[0] == '|') 
        {
            token = strtok_r(NULL, " ", &savePointer);
            continue;
        }
        else if (strcmp(token, col_name) == 0) 
        {
            col_loc = total_col;
            skip = 1;
        }
        else if (token[0] == '(' && skip == 0)
        {
            if (token[1] == 's') strcpy(data_type[total_col], "string");
            else if (token[1] == 'c') strcpy(data_type[total_col], "char");
            else if (token[1] == 'i') strcpy(data_type[total_col], "int");
            printf("data type = %s\n", data_type[total_col]);

            total_col++;
        }
        else if (skip == 1) skip = 0;
        else
        {
            strcpy(columns[total_col], token);
            printf("columns = %s\n", columns[total_col]);
        }
        token = strtok_r(NULL, " ", &savePointer);

    }
    if (col_loc == -1) 
    {
        fclose(table);
        return 0; // column not found
    }

    fgets(line, sizeof(line), table);
    int checker = generate_table(table_path, columns, data_type, total_col);
    int row_counter = 0;
    while (fgets(line, sizeof(line), table))
    {
        int data_counter = 0;
        int found = 0;
        token = strtok_r(line, " ", &savePointer);

        while(token != NULL)
        {
            if (strcmp(token, "|") == 0 || token[0] == '(') 
            {
                token = strtok_r(NULL, " ", &savePointer);
                continue;
            }
            else if (data_counter == col_loc && found == 0)
            {
                found = 1;
            }
            else 
            {
                strcpy(data[row_counter][data_counter], token);
                data_counter++;
            }
            token = strtok_r(NULL, " ", &savePointer);
        }
        row_counter++;
    }
    
    fclose(table);
    int insert_check;
    if (row_counter != 0)
    {
        for(int l = 0; l < row_counter; l++)
        {
            insert_check = insert_row(table_path, data[l]);
        }
    }


    return 1;
}

void drop_table(char *table_path)
{
    char drop_table[1024];
    memset(drop_table, 0, sizeof drop_table);
    sprintf(drop_table, "%s %s", "rm", table_path);

    system(drop_table);
}

void print_log(char *username, char *command){
    FILE *log_file = fopen("./log.txt", "a");
    char logText[1000];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    if (strcmp(username, "@") == 0){
        sprintf(logText, "%d-%02d-%02d %02d:%02d:%02d:root:%s\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, command);
    }
    else sprintf(logText, "%d-%02d-%02d %02d:%02d:%02d:%s:%s\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, command);
    fputs(logText, log_file);

    fclose(log_file);
}

void parse_command(char *command, char *use_database){
    FILE *history;
    char path[1024];
    char text[1024];

    strcpy(path, rpath);
    strcat(path, "/history/");
    strcat(path, use_database);
    history = fopen(path, "a");

    char *token;
    token = strtok(command, ";");

    while (token != NULL){
        memset(text, 0, sizeof text);
        if (token[0] == ' '){
            memmove(token, token+1, strlen(token));
        }
        printf("%s;\n", token);
        sprintf(text, "%s;\n", token);
        fputs(text, history);
        token = strtok(NULL, ";");
    }

    fclose(history);
}

int count_column(char *table_path)
{    
    FILE *table = fopen(table_path, "r");
    char line[2048]; 
    char *saveptr, *iterator;
    int counter = 0, tok_counter = 0;

    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    iterator = strtok_r(line, " ", &saveptr);
    while (iterator != NULL)
    {
        if(iterator[0] == '|' || iterator[0] == '(') 
        {
            iterator = strtok_r(NULL, " ", &saveptr);
        }
        else 
        {
            counter++;
            iterator = strtok_r(NULL, " ", &saveptr);
        }
    }

    return counter;
}

int find_col(char *table_path, char *col_name)
{
    char line[2048];
    char *savePointer;
    char columns[100][100];
    char data_type[100][100];
    char data[1024][100][60];
    int total_col = 0;
    int col_loc = -1;
    FILE *table = fopen(table_path, "r");

    // Searching for column
    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    char *token = strtok_r(line, " ", &savePointer);
    while(token != NULL)
    {
        //printf("token = %c\n", token[0]);
        if(token[0] == '|') 
        {
            token = strtok_r(NULL, " ", &savePointer);
            continue;
        }
        else if (strcmp(token, col_name) == 0) 
        {
            col_loc = total_col + 1;
            break;
        }
        else if (token[0] == '(')
        {
            total_col++;
        }

        token = strtok_r(NULL, " ", &savePointer);
    }

    if (col_loc == -1) 
    {
        fclose(table);
        return 0; // column not found
    }
    else return col_loc;
}

int update(char *table_path, char *newValue, int colNum)
{
    FILE *table;
    int length;
    char *savePtr;
    char err_msg[1024] = {0};
    char readLine[2084] = {0};

    table = fopen(table_path, "r");
    
    fgets(readLine, sizeof(readLine), table);
    fgets(readLine, sizeof(readLine), table);

    char *token = strtok_r(readLine, "(", &savePtr);

    for(int i = 0; i < colNum; i++)
    {
        token = strtok_r(readLine, "(", &savePtr);
    }

    if(token[0] == 's')
    {
        length = 50;
        if (strlen(newValue) > length) 
        {
            fclose(table);
            return 11;
        }
        
        if(newValue[0] != '\'' || newValue[strlen(newValue) - 1] != '\'') // alfabet tanpa tanda petik
        {
            fclose(table);
            return 16;
        }
    }
    else if(token[0] == 'c')
    {
        length = 20;
        if (strlen(newValue) > length) 
        {
            fclose(table);
            return 12;
        }
        
        if(newValue[0] != '\'' || newValue[strlen(newValue) - 1] != '\'') // alfabet tanpa tanda petik
        {
            fclose(table);
            return 15;
        }
    }
    else if (token[0] == 'i') // int
    {
        length = 20;
        if (strlen(newValue) > length)
        {
            fclose(table);
            return 13;
        }
        for (int i = 0; i < strlen(newValue); i++)
        {
            if (newValue[i] < 48 || newValue[i] > 57) 
            {
                fclose(table);
                return 14;
            }
        }
    }

    fgets(readLine, sizeof(readLine), table);

    while(fgets(readLine, sizeof(readLine), table))
    {
        char *token = strtok_r(readLine, " ", &savePtr);
        int counter = 0;
        while(token != NULL)
        {
            //printf("token = %c\n", token[0]);
            if(token[0] != '|') 
            {
                counter++;
            }

            if(counter == colNum)
            {
                
            }

            token = strtok_r(NULL, " ", &savePtr);
        }
    }

}

void print_all(const char *file_path)
{
    FILE *file = fopen(file_path, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char line[1024];
    while (fgets(line, sizeof(line), file)) {
        printf("%s", line);
    }

    fclose(file);
}

void print_columns(const char *file_path, const char **columns, int num_columns)
{
    FILE *file = fopen(file_path, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char header[1024];
    fgets(header, sizeof(header), file); // Read the header line

    int column_indices[num_columns];
    memset(column_indices, -1, sizeof(column_indices));

    char *token = strtok(header, "|");
    int index = 0;

    // Find the indices of the specified columns
    while (token != NULL) {
        for (int i = 0; i < num_columns; i++) {
            if (strcmp(token, columns[i]) == 0) {
                column_indices[i] = index;
            }
        }
        token = strtok(NULL, "|");
        index++;
    }

    // Check if all specified columns are found
    for (int i = 0; i < num_columns; i++) {
        if (column_indices[i] == -1) {
            printf("Column '%s' not found.\n", columns[i]);
            fclose(file);
            return;
        }
    }

    char line[1024];
    while (fgets(line, sizeof(line), file)) {
        char *value = strtok(line, "|");
        index = 0;

        // Traverse the values and print the values at the desired column indices
        for (int i = 0; i < num_columns; i++) {
            while (value != NULL) {
                if (index == column_indices[i]) {
                    printf("%s\t", value);
                    break;
                }
                value = strtok(NULL, "|");
                index++;
            }
            // Reset index for the next column
            index = 0;
            // Move to the next value in the line
            value = strtok(NULL, "|");
        }
        printf("\n");
    }

    fclose(file);
}


int delete_table(const char* filename) {
    const int MAX_LINE_LENGTH = 1024;
    const int MAX_LINES_TO_KEEP = 3;

    // Open the file for reading
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Read and store the first three lines
    char** linesToKeep = malloc(MAX_LINES_TO_KEEP * sizeof(char*));
    int lineCount = 0;
    char buffer[MAX_LINE_LENGTH];

    while (lineCount < MAX_LINES_TO_KEEP && fgets(buffer, MAX_LINE_LENGTH, file) != NULL) {
        int lineLength = strlen(buffer);
        linesToKeep[lineCount] = malloc((lineLength + 1) * sizeof(char));
        strcpy(linesToKeep[lineCount], buffer);
        lineCount++;
    }

    // Close the file
    fclose(file);

    // Open the file for writing
    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Write the stored lines back to the file
    for (int i = 0; i < lineCount; i++) {
        fputs(linesToKeep[i], file);
        free(linesToKeep[i]);
    }
    free(linesToKeep);

    // Close the file
    fclose(file);
    return 1;
}


int select_columns(const char* filename, const char columns[][100], int numColumns) {
    const int MAX_LINE_LENGTH = 1024;
    const char* COLUMN_SEPARATOR = "|";
    const char* HEADER_SEPARATOR = "-";

    // Open the file for reading
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Skip the first line
    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read the header line
    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Find the starting positions of the selected columns
    int columnStartPos[numColumns];
    for (int i = 0; i < numColumns; i++) {
        char* columnStart = strstr(headerLine, columns[i]);
        if (columnStart != NULL) {
            columnStartPos[i] = columnStart - headerLine;
        } else {
            printf("Column not found: %s\n", columns[i]);
            fclose(file);
            return 0;
        }
    }

    // Create the new table header
    for(int i = 0; i <= numColumns*24; i++) {
        printf("-");
    }
    puts("");

    for (int i = 0; i < numColumns; i++) {
        printf("| %-20s ", columns[i]);
    }
    printf("|\n");

    // Print the header separator line
    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    for(int i = 0; i <= numColumns*24; i++) {
        printf("-");
    }
    puts("");

    // Read and print the data lines
    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        for (int i = 0; i < numColumns; i++) {
            int startPos = columnStartPos[i];

            // Find the end position of the column value
            int endPos = startPos;
            while (dataLine[endPos] != '|' && dataLine[endPos] != '\n') {
                endPos++;
            }

            printf("| ");
            int n = 0;
            for (int k = startPos; k < endPos; k++) {
                if (dataLine[k] != ' ' && dataLine[k] != '\t') {
                    printf("%c", dataLine[k]);
                    n++;
                }
            }
            for(int i = 0; i <= 20-n; i++) {
                printf(" ");
            }
        }
        printf("|\n");
    }

    // Close the file
    fclose(file);
    return 1;
}

int updateColumn(const char* filename, const char* columnName, const char* value) {
    const int MAX_LINE_LENGTH = 1024;
    const char* COLUMN_SEPARATOR = "|";
    const char* HEADER_SEPARATOR = "-";

    // Open the file for reading and writing
    FILE* file = fopen(filename, "r+");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Skip the first line
    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read the header line
    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Find the starting position of the selected column
    int columnStartPos;
    char* columnStart = strstr(headerLine, columnName);
    if (columnStart == NULL) {
        printf("Column not found: %s\n", columnName);
        fclose(file);
        return 0;
    }
    else {
        columnStartPos = columnStart - headerLine;
    }

    // Skip the separator line
    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read and update the data lines
    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        // printf("dataline : %s\n", dataLine);
        // Find the starting position of the column value
        char* columnValueStart = dataLine + columnStartPos;
        // printf("cvs : %s\nvalue : %s\n", columnValueStart,value);

        // Check if the current line already has the same value
        int isEqual = strncmp(columnValueStart, value, strlen(value)) == 0;
        if (isEqual) {
            continue;  // Skip to the next line
        }

        // Update the column value with the new value
        int valueIndex = 0;
        while (value[valueIndex] != '\0' || *columnValueStart != '\0' && *columnValueStart != ' ') {
            // printf("cvs : %c || value : %c ", *columnValueStart,value[valueIndex]);
            *columnValueStart = value[valueIndex];
            columnValueStart++;
            valueIndex++;
        }

        // If there are remaining characters in the data line, but the value has been exhausted, replace them with whitespace
        while (*columnValueStart != '\0' && *columnValueStart != ' ') {
            *columnValueStart = ' ';
            columnValueStart++;
        }

        // Move the file pointer back to the beginning of the current line
        fseek(file, -strlen(dataLine), SEEK_CUR);

        // Write the updated data line to the file
        fputs(dataLine, file);
        continue;
        // Move the file pointer to the beginning of the next line
        fseek(file, strlen(dataLine), SEEK_CUR);
    }

    // Close the file
    fclose(file);
    printf("Column has been updated.\n");
    return 1;
}

void trimWhiteSpace(char* str) {
    // Trim leading white spaces
    char* start = str;
    while (*start && isspace((unsigned char)*start)) {
        ++start;
    }

    // Trim trailing white spaces
    char* end = str + strlen(str) - 1;
    while (end > start && isspace((unsigned char)*end)) {
        --end;
    }
    *(end + 1) = '\0';

    // Shift the trimmed string to the beginning
    if (str != start) {
        memmove(str, start, end - start + 2);
    }
}

int updateColumnWithCondition(const char* filename, const char* columnName, const char* value, const char* conditionColumn, const char* conditionValue) {
    const int MAX_LINE_LENGTH = 1024;
    const char* COLUMN_SEPARATOR = "|";
    const char* HEADER_SEPARATOR = "-";

    // Open the file for reading and writing
    FILE* file = fopen(filename, "r+");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Skip the first line
    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read the header line
    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Find the starting positions of the selected column and condition column
    int columnStartPos;
    int conditionColumnStartPos;
    char* columnStart = strstr(headerLine, columnName);
    char* conditionColumnStart = strstr(headerLine, conditionColumn);
    if (columnStart == NULL) {
        printf("Column not found: %s\n", columnName);
        fclose(file);
        return 0;
    } else if (conditionColumnStart == NULL) {
        printf("Condition column not found: %s\n", conditionColumn);
        fclose(file);
        return 0;
    } else {
        columnStartPos = columnStart - headerLine;
        conditionColumnStartPos = conditionColumnStart - headerLine;
    }
    printf("column start pos : %d\n", columnStartPos);
    printf("condition column start pos : %d\n", conditionColumnStartPos);

    // Skip the separator line
    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read and update the data lines
    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        // printf("dataline : %s\n", dataLine);
        // Find the starting positions of the column value and condition value
        char* columnValueStart = dataLine + columnStartPos;
        char* conditionValueStart = dataLine + conditionColumnStartPos;
        // printf("column value before : %s\n", columnValueStart);
        // printf("condition value before : %s\n", conditionValueStart);

        // Extract the column value and condition value
        char columnValue[MAX_LINE_LENGTH];
        char condValue[MAX_LINE_LENGTH];
        sscanf(columnValueStart, "%[^|]", columnValue);
        sscanf(conditionValueStart, "%[^|]", condValue);
        // printf("column value after : %s\n", columnValueStart);
        // printf("condition value after : %s\n", conditionValueStart);
        // printf("column value : %s\n", columnValue);
        // printf("condition value: %s\n", condValue);

        // Check if the current row satisfies the condition
        trimWhiteSpace(condValue);
        int conditionSatisfied = strcmp(condValue, conditionValue) == 0;
        if (!conditionSatisfied) {
            // printf("sama cuy cond valuenya || %s = %s\n", condValue, conditionValue);
            continue;  // Skip to the next line
        }

        // Check if the current line already has the same value for the column
        int isEqual = strcmp(columnValue, value) == 0;
        if (isEqual) {
            continue;  // Skip to the next line
        }

        // Update the column value with the new value
        strncpy(columnValueStart, value, strlen(value));

        // If the new value is shorter than the original value, fill the remaining characters with whitespace
        int newValueLength = strlen(value);
        int originalValueLength = strlen(columnValue);
        if (newValueLength < originalValueLength) {
            int remainingWhitespace = originalValueLength - newValueLength;
            memset(columnValueStart + newValueLength, ' ', remainingWhitespace);
        }

        // Move the file pointer back to the beginning of the current line
        fseek(file, -strlen(dataLine), SEEK_CUR);

        // Write the updated data line to the file
        fputs(dataLine, file);
        continue;
        // Move the file pointer to the beginning of the next line
        fseek(file, strlen(dataLine), SEEK_CUR);
    }

    // Close the file
    fclose(file);
    printf("Column has been updated.\n");
    return 1;
}


int select_columns_with_condition(const char* filename, const char columns[][100], int numColumns, const char* conditionColumn, const char* conditionValue) {
    const int MAX_LINE_LENGTH = 1024;
    const char* COLUMN_SEPARATOR = "|";
    const char* HEADER_SEPARATOR = "-";

    // Open the file for reading
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    // Skip the first line
    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Read the header line
    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    // Find the starting positions of the selected columns
    int columnStartPos[numColumns];
    for (int i = 0; i < numColumns; i++) {
        char* columnStart = strstr(headerLine, columns[i]);
        if (columnStart != NULL) {
            columnStartPos[i] = columnStart - headerLine;
        } else {
            printf("Column not found: %s\n", columns[i]);
            fclose(file);
            return 0;
        }
    }

    // Find the starting position of the condition column
    int conditionColumnStartPos = -1;
    char* conditionColumnStart = strstr(headerLine, conditionColumn);
    if (conditionColumnStart != NULL) {
        conditionColumnStartPos = conditionColumnStart - headerLine;
    } else {
        printf("Condition column not found: %s\n", conditionColumn);
        fclose(file);
        return 0;
    }

    // Create the new table header
    for (int i = 0; i <= numColumns*24; i++) {
        printf("-");
    }
    puts("");

    for (int i = 0; i < numColumns; i++) {
        printf("| %-20s ", columns[i]);
    }
    printf("|\n");

    // Print the header separator line
    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table: %s\n", filename);
        fclose(file);
        return 0;
    }

    for (int i = 0; i <= numColumns*24; i++) {
        printf("-");
    }
    puts("");

    // Read and print the data lines
    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        // printf("dataline : %s\n", dataLine);
        // Find the starting position of the condition value
        char* conditionValueStart = dataLine + conditionColumnStartPos;

        // Extract the condition value
        char conditionValueFromData[MAX_LINE_LENGTH];
        sscanf(conditionValueStart, "%[^|]", conditionValueFromData);
        trimWhiteSpace(conditionValueFromData);

        // Check if the current row satisfies the condition
        int conditionSatisfied = strcmp(conditionValueFromData, conditionValue) == 0;
        if (!conditionSatisfied) {
            continue;  // Skip to the next line
        }

        // Print the selected columns for the current line
        for (int i = 0; i < numColumns; i++) {
            int startPos = columnStartPos[i];

            // Find the end position of the column value
            int endPos = startPos;
            while (dataLine[endPos] != '|' && dataLine[endPos] != '\n') {
                endPos++;
            }

            printf("| ");
            int n = 0;
            for (int k = startPos; k < endPos; k++) {
                if (dataLine[k] != ' ' && dataLine[k] != '\t') {
                    printf("%c", dataLine[k]);
                    n++;
                }
            }
            for(int i = 0; i <= 20-n; i++) {
                printf(" ");
            }
        }
        printf("|\n");
    }

    // Close the file
    fclose(file);
    return 1;
}

int print_all_with_condition(const char* file_path, const char* conditionColumn, const char* conditionValue) {
    const int MAX_LINE_LENGTH = 1024;

    FILE* file = fopen(file_path, "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return 0;
    }

    // Skip the first line
    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", file_path);
        fclose(file);
        return 0;
    }

    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table.\n");
        fclose(file);
        return 0;
    }

    // Find the starting position of the condition column
    int conditionColumnStartPos = -1;
    char* conditionColumnStart = strstr(headerLine, conditionColumn);
    if (conditionColumnStart != NULL) {
        conditionColumnStartPos = conditionColumnStart - headerLine;
    } else {
        printf("Condition column not found: %s\n", conditionColumn);
        fclose(file);
        return 0;
    }

    printf("%s", headerLine);  // Print the header line

    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table.\n");
        fclose(file);
        return 0;
    }

    printf("%s", separatorLine);  // Print the separator line

    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        // Find the starting position of the condition value
        char* conditionValueStart = dataLine + conditionColumnStartPos;

        // Extract the condition value
        char conditionValueFromData[MAX_LINE_LENGTH];
        sscanf(conditionValueStart, "%[^|]", conditionValueFromData);
        trimWhiteSpace(conditionValueFromData);

        // Check if the current row satisfies the condition
        int conditionSatisfied = strcmp(conditionValueFromData, conditionValue) == 0;
        if (conditionSatisfied) {
            printf("%s", dataLine);
        }
    }

    fclose(file);
    return 1;
}

int delete_table_with_condition(const char* filename, const char* conditionColumn, const char* conditionValue) {
    const int MAX_LINE_LENGTH = 1024;
    const int MAX_LINES_TO_KEEP = 3;

    // Open the file for reading and writing
    FILE* file = fopen(filename, "r+");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return 0;
    }

    char skipLine[MAX_LINE_LENGTH];
    if (fgets(skipLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Empty file: %s\n", filename);
        fclose(file);
        return 0;
    }

    char headerLine[MAX_LINE_LENGTH];
    if (fgets(headerLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table.\n");
        fclose(file);
        return 0;
    }

    int conditionColumnStartPos = -1;
    char* conditionColumnStart = strstr(headerLine, conditionColumn);
    if (conditionColumnStart != NULL) {
        conditionColumnStartPos = conditionColumnStart - headerLine;
        // printf("condition column pos : %d\n", conditionColumnStartPos);
    } else {
        printf("Condition column not found: %s\n", conditionColumn);
        fclose(file);
        return 0;
    }

    char separatorLine[MAX_LINE_LENGTH];
    if (fgets(separatorLine, MAX_LINE_LENGTH, file) == NULL) {
        printf("Incomplete table.\n");
        fclose(file);
        return 0;
    }

    // Create a temporary file to store the modified contents
    const char* tempFilename = "temp.txt";
    FILE* tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return 0;
    }

    // Write the header and separator lines to the temporary file
    fputs(skipLine, tempFile);
    fputs(headerLine, tempFile);
    fputs(separatorLine, tempFile);

    // Read and delete the remaining data lines based on the condition
    char dataLine[MAX_LINE_LENGTH];
    while (fgets(dataLine, MAX_LINE_LENGTH, file) != NULL) {
        // printf("dataline : %s\n", dataLine);
        // Find the starting position of the condition value
        char* conditionValueStart = dataLine + conditionColumnStartPos;

        // Extract the condition value
        char conditionValueFromData[MAX_LINE_LENGTH];
        sscanf(conditionValueStart, "%[^|]", conditionValueFromData);
        trimWhiteSpace(conditionValueFromData);

        // Check if the current row satisfies the condition
        int conditionSatisfied = strcmp(conditionValueFromData, conditionValue) == 0;
        // printf("%s = %s\n", conditionValueFromData, conditionValue);
        if (!conditionSatisfied) {
            // Write the line to the temporary file
            fputs(dataLine, tempFile);
        }
    }

    // Close the files
    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    if (remove(filename) != 0) {
        printf("Failed to delete file: %s\n", filename);
        return 0;
    }
    if (rename(tempFilename, filename) != 0) {
        printf("Failed to rename file: %s\n", tempFilename);
        return 0;
    }

    return 1;
}




int main(int argc, char const *argv[]) 
{
    printf("waiting for client...\n");
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char username[60];
    char use_database[100] = "";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) 
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) 
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // Checking whether client is sudo
    int sudo = 0;
    char sudo_checker[10] = {0};
    read(new_socket, sudo_checker, 1024);
    if(sudo_checker[0] == 's') sudo = 1;

    // Making directory /databases to contain all the database
    database_setup();

    printf("\n-------Hello from server side--------\n");

    // Receiving username
    if (sudo == 0) {
        read(new_socket, username, 60);
        if(strcmp(username, "Log in failed") == 0) 
        {
            printf("%s.\n", username);
            exit(1);
        }
        else printf("Logged in with %s.\n", username);
    }
    else {
        printf("Logged in with root.\n");
        strcpy(username, "root");
    }

    // Checking for commands
    while(1){
        char msg[1024];
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);
        char *command = buffer;

        if (command != NULL){
            if (strlen(use_database) != 0){
                parse_command(command, use_database);
            }
            print_log(username, command);
        }
        
        char *token = strtok(command, " ");

        if (token == NULL) {
            strcpy(msg, "Invalid Command.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }

        if (strcmp(token, "QUIT") == 0 || strcmp(token, "quit") == 0) {
            printf("Goodbye from server.\n");
            strcpy(msg, "Shutting down databases...");
            send(new_socket, msg, strlen(msg), 0);
            break;
            return -1;
        }
        // Authorization
        if (strcmp(token, "GRANT") == 0)
        {
            if(sudo == 0)
            {
                strcpy(msg, "Root access is needed to be able to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            char tok_database[100];
            char tok_user[100];
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "PERMISSION") == 0)
            {
                token = strtok(NULL, " ");
                if (token != NULL)
                {
                    strcpy(tok_database, token);
                    token = strtok(NULL, " ");
                    if (token != NULL && strcmp(token, "INTO") == 0)
                    {
                        token = strtok(NULL, " ");
                        if (token != NULL)
                        {
                            strcpy(tok_user, token);
                            int check;

                            check = check_dbaccess(tok_database, tok_user);

                            if (check == 2)
                            {
                                // masukkin row baru ke database_access
                                char path[1024];
                                char col[100][60] = {0};
                                strcpy(path, rpath);
                                strcat(path, "/users/database_access");

                                strcpy(col[0], tok_user);
                                strcpy(col[1], tok_database);

                                int insert = insert_row_user(path, col);
                                 printf("%s %s\n", col[0], col[1]);
                                if(insert == 1) {
                                    strcpy(msg, "Successfully granted a new access to the specified user.");
                                }
                                else {
                                    strcpy(msg, "Unable to grant permission.");
                                }

                                // if(insert_row(path, col, 2) == 1) strcpy(msg, "Successfully granted a new access to the specified user.");
                                // else {
                                //     printf("%s\n", path);
                                //     strcpy(msg, "Unable to grant permission.");
                                // };
                            }
                            else if (check == 1) strcpy(msg, "The user already has access to the database.");
                            else if (check == 3) strcpy(msg, "Specified user not exist.");
                            else if (check == 0) strcpy(msg, "Unable to find database.");
                        }
                        else strcpy(msg, "Unable to grant permission.");
                    }
                    else strcpy(msg, "Unable to grant permission.");
                }
                else strcpy(msg, "Unable to grant permission.");
            }
            else strcpy(msg, "Unable to grant permission.");

            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        else if (strcmp(token, "USE") == 0)
        {
            FILE *history;
            char path[1024];


            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to create.");
            }
            else if(sudo == 1) 
            {
                sprintf(msg, "Database %s is now in use.", token);
                strcpy(use_database, token);

                strcpy(path, rpath);
                strcat(path, "/history/");
                strcat(path, use_database);
                history = fopen(path, "a");
                fclose(history);
            }
            else 
            {
                int checker;
                checker = check_dbaccess(token, username);

                if(checker == 1 || sudo == 1) 
                {
                    sprintf(msg, "Database %s is now in use.", token);
                    strcpy(use_database, token);
                }
                else if(checker == 2)
                {
                    sprintf(msg, "User %s cannot access this database.", username);
                } 
                else strcpy(msg, "Database not found.");
            }
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        // DDL
        else if (strcmp(token, "CREATE") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to create.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "USER") == 0)
            {
                if(sudo)
                {
                    int check;
                    check = create_user(token);

                    if (check == 1) strcpy(msg, "A new user has been created.");
                    else if (check == 2) strcpy(msg, "Unable to create new user (table doesn't exist).");
                    else if (check == 3) strcpy(msg, "User already exist.");
                    else if(check == 0) strcpy(msg, "Unable to create a new user.");
                    else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
                    else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
                    else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
                    else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter");
                }
                else
                {
                    strcpy(msg, "Root access is required to create a new user.");
                }

                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "DATABASE") == 0){
                token = strtok(NULL, " ");
                if (token == NULL)
                {
                    strcpy(msg, "Unable to create database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                int check;
                check = check_dbaccess(token, username);

                if(check == 1 || check == 2) strcpy(msg, "Database already exist.");
                else
                {
                    check = create_database(token);
                    if(check == -1 && check == 0)
                    {
                        strcpy(msg, "Unable to create database.");
                    }
                    else 
                    {
                        if(sudo == 0)
                        {
                            // masukkin row baru ke database_access
                            char path[1024];
                            char col[100][60] = {0};
                            int insert;
                            strcpy(path, rpath);
                            strcat(path, "/users/database_access");

                            
                            strcpy(col[0], username);
                            strcpy(col[1], token);

                            insert = insert_row(path, col);

                            if(insert == 1) strcpy(msg, "Database has been created.");
                            else if (insert == 11) strcpy(msg, "Name of database can not be more than 50 characters.");;
                        }
                        else {
                            
                            strcpy(msg, "Database has been created.");
                        }
                    }
                }
                printf("\n%s\n", msg);
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "TABLE") == 0){
                int checker;
                char column_name[100][100];
                char data_type[100][100];
                int counter = 0;
                token = strtok(NULL, " "); // -> table1
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                printf("used database %s\n", use_database);
                if (strcmp(use_database, "") == 0) {
                    strcpy(msg, "No database is used.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_path[1024] = "../databases/";
                strcat(table_path, use_database); // -> $use_database mengikuti authorisasi
                strcat(table_path, "/");
                strcat(table_path, token);

                // mengecek apakah tabel sudah ada atau belum
                checker = find_table(table_path);
                if (checker == 51)
                {
                    strcpy(msg, "Table already exist.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, "("); // -> kolom1 string, kolom2 int)
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_details[1024];
                strcpy(table_details, token);

                token = strtok(table_details, " "); // -> kolom1
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                while(token != NULL){
                    strcpy(column_name[counter], token);

                    token = strtok(NULL, " "); // -> string -> int
                    if (token == NULL) 
                        break;

                    
                    strcpy(data_type[counter], token);

                    if (data_type[counter][strlen(data_type[counter])-1] == ')' || data_type[counter][strlen(data_type[counter])-1] == ',')
                        data_type[counter][strlen(data_type[counter])-1] = '\0';

                    printf("nama kolom = %s\n", column_name[counter]);
                    printf("tipe data = %s\n", data_type[counter]);

                    counter++;
                    token = strtok(NULL, " "); // -> kolom2 -> NULL
                }
                checker = generate_table(table_path, column_name, data_type, counter);
                if (checker == 1) strcpy(msg, "Table has been created.");
                else if(checker == 11) strcpy(msg, "String column name can not be more than 41 characters.");
                else if(checker == 12) strcpy(msg, "Char column name can not be more than 13 characters.");
                else if(checker == 13) strcpy(msg, "Int column name can not be more than 14 characters.");
                else if(checker == 14) strcpy(msg, "Invalid data type.");

                send(new_socket, msg, strlen(msg), 0);
                continue;
            }    
            else 
            {
                strcpy(msg, "Unable to create.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }   
        }
        else if (strcmp(token, "DROP") == 0)
        {
            printf("------drop-----\n");

            token = strtok(NULL, " ");
            if (token == NULL)
            {
                strcpy(msg, "Unable to perform drop command.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }

            if (strcmp(token, "DATABASE") == 0)
            {
                printf("-----database-----\n");

                int checker;
                int access = 0;

                token = strtok(NULL, " ");
                if (token == NULL) {
                    strcpy(msg, "Unable to drop database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                // Checking access & checking whether database exist
                if (sudo) access = 1;
                else {
                    checker = check_dbaccess(token, username);
                    access = checker;
                }

                if (access == 1)
                {
                    char database_path[200];
                    strcpy(database_path, rpath);
                    strcat(database_path, "/");
                    strcat(database_path, token);

                    char drop_database[1024];
                    memset(drop_database, 0, sizeof drop_database);
                    sprintf(drop_database, "%s %s", "rm -rf", database_path);

                    system(drop_database);

                    if(strcmp(use_database, token) == 0) strcpy(use_database, "");

                    strcpy(msg, "Successfully dropped database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else if (access = 2){
                    sprintf(msg, "User %s unable to drop this database.", username);
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else if (access = 0){
                    strcpy(msg, "Database not exist.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }
            else if (strcmp(token, "TABLE") == 0)
            {
                printf("-----table-----\n");
                int checker = 0;

                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to drop database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                if(strcmp(use_database, "") == 0) 
                {
                    strcpy(msg, "No database is used.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_path[200];
                strcpy(table_path, rpath);
                strcat(table_path, "/");
                strcat(table_path, use_database);
                strcat(table_path, "/");
                strcat(table_path, token);

                checker = find_table(table_path);
                if (checker == 51)
                {
                    drop_table(table_path);

                    strcpy(msg, "Successfully dropped the table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else
                {
                    strcpy(msg, "No table found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }   
            else if (strcmp(token, "COLUMN") == 0)
            {
                int checker;
                char target_col[60];
                token = strtok(NULL, " ");
                if (token == NULL)
                {
                    strcpy(msg, "Unable to drop column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                strcpy(target_col, token);
                token = strtok(NULL, " ");

                if (token == NULL || strcmp(token, "FROM") != 0)
                {
                    strcpy(msg, "Unable to drop column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, " ");
                char table_path[200];
                strcpy(table_path, rpath);
                strcat(table_path, "/");
                strcat(table_path, use_database);
                strcat(table_path, "/");
                strcat(table_path, token);
                // Checking the existance of the table
                checker = find_table(table_path);
                if (checker == 51) // table found
                {
                    checker = drop_column(table_path, token, target_col);
                    if (checker == 0) strcpy(msg, "No column found.");
                    else if (checker == 1) strcpy(msg, "Column successfully deleted.");
                    send(new_socket, msg, strlen(msg), 0);

                }
                else
                {
                    strcpy(msg, "No table found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }
        }
        // DML
        else if (strcmp(token, "INSERT") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to insert column.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "INTO") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to insert column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else 
                {
                    char table_path[200];
                    strcpy(table_path, rpath);
                    strcat(table_path, "/");
                    strcat(table_path, use_database);
                    strcat(table_path, "/");
                    strcat(table_path, token);

                    // Checking whether table exist in the database or not
                    int checker;
                    checker = find_table(table_path);
                    if (checker == 51)
                    {
                        // counting column in the specified table
                        int numOfCol;
                        numOfCol = count_column(table_path);

                        // parse values
                        if (token == NULL) 
                        {
                            strcpy(msg, "Unable to insert column.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        char data[100][60];
                        memset(data, 0, sizeof data);
                        for(int i = 0; i < numOfCol; i++)
                        {
                            int dataChar = 0;
                            token = strtok(NULL, ",");
                            if(i != 0 && token == NULL) // kekurangan data
                            {
                                strcpy(msg, "Insufficient argument. Unable to perform insert.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            int len = strlen(token);
                            for(int j = 0; j < len; j++)
                            {
                                if( j == 0 && ( token[j] == '(' || token[j] == ' ' ) ||
                                    j == len - 1 && token[j] == ')')
                                {
                                    continue;
                                }
                                else if((token[j] < 48 || token[j] > 57) && j != 0 && j != len-1 &&
                                        ( !(token[1] == '\'' && token[len - 1] == '\'') && 
                                         !(token[1] == '\'' && token[len - 2] == '\'' && token[len - 1] == ')'))) // alfabet tanpa tanda petik
                                {
                                    strcpy(msg, "String or char data type need ' in between.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else 
                                {
                                    data[i][dataChar] = token[j];
                                    dataChar++;
                                }
                            }
                            printf("%s\n", data[i]);
                        }
                        token = strtok(NULL, ",");
                        if (token != NULL) // kelebihan data
                        {
                            strcpy(msg, "Excessive argument. Unable to perform insert.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        int check = insert_row(table_path, data);

                        if (check == 1) strcpy(msg, "A new record has been inserted.");
                        else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
                        else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
                        else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
                        else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter.");
                        else if(check == 15) strcpy(msg, "Input of char data type need ' in between.");
                        else if(check == 16) strcpy(msg, "Input of string data type need ' in between.");

                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                            
                    }
                    else
                    {
                        strcpy(msg, "No table found.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }

                }
            }
        }
        else if (strcmp(token, "UPDATE") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to update table.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }

            char table_path[200];
            strcpy(table_path, rpath);
            strcat(table_path, "/");
            strcat(table_path, use_database);
            strcat(table_path, "/");
            strcat(table_path, token);
            int table_check = find_table(table_path);

            if (table_check == 51)
            {
                token = strtok(NULL, " ");
                if (token == NULL || strcmp(token, "SET") != 0) 
                {
                    strcpy(msg, "Unable to update table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to update table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                // find kolom
                char col_equal[60], value [60];
                memset(col_equal, 0, sizeof col_equal);
                // token = strtok(NULL, "=");
                printf("%s\n", col_equal);

                int kolom_check = find_col(table_path, token);
                if (kolom_check == 0)
                {
                    strcpy(msg, "No column found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                strcpy(col_equal, token);

                // value
                token = strtok(NULL, "= ");
                if (token == NULL) 
                {
                    strcpy(msg, "Invalid format.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                strcpy(value, token);

                // where
                token = strtok(NULL, " ");
                printf("%s\n%s\n%s\n", table_path, col_equal, value);
                if (token != NULL) 
                {
                    if (strcmp(token, "WHERE") == 0)
                    {
                        token = strtok(NULL, " ");
                        if (token == NULL) 
                        {
                            strcpy(msg, "Invalid format.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                        // find condition column
                        char col_condition[60];
                        strcpy(col_condition, token);
                        // find condition value
                        token = strtok(NULL, "= ");
                        if (token == NULL) 
                        {
                            strcpy(msg, "Invalid format.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                        printf("%s %s %s %s %s\n", table_path, col_equal, value, col_condition, token);
                        int updatewhere = updateColumnWithCondition(table_path, col_equal, value, col_condition, token);
                        if(updatewhere == 1) {
                            strcpy(msg, "Successfully update the column.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                        else {
                            strcpy(msg, "Update column has failed.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                    }
                    else
                    {
                        strcpy(msg, "Invalid format.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                }
                else 
                {
                    int update = updateColumn(table_path, col_equal, value);
                    if(update == 1) {
                        strcpy(msg, "Successfully update the column.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                    else {
                        strcpy(msg, "Update column has failed.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                }
            }
            else if (table_check == 50)
            {
                strcpy(msg, "No table found.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
        }
        else if (strcmp(token, "SELECT") == 0) {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to perform select.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "*") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to perform select.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else if(strcmp(token, "FROM") == 0) {
                    token = strtok(NULL, " ");
                    if (token == NULL) 
                    {
                        strcpy(msg, "Unable to perform select.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                    else {
                        char table_path[200];
                        strcpy(table_path, rpath);
                        strcat(table_path, "/");
                        strcat(table_path, use_database);
                        strcat(table_path, "/");
                        strcat(table_path, token);

                        int check_table = find_table(table_path);

                        if(check_table == 51) {
                            token = strtok(NULL, " ");
                            if (token == NULL) 
                            {
                                print_all(table_path);
                                strcpy(msg, "Table content has been printed.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            else if(strcmp(token, "WHERE") == 0) {
                                token = strtok(NULL, " ");
                                if (token == NULL) 
                                {
                                    strcpy(msg, "Unable to perform select.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                char col_condition[60];
                                strcpy(col_condition, token);
                                // find condition value
                                token = strtok(NULL, "= ");
                                if (token == NULL) 
                                {
                                    strcpy(msg, "Invalid format.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                printf("%s %s %s\n", table_path, col_condition, token);
                                int selectallwhere = print_all_with_condition(table_path, col_condition, token);
                                if(selectallwhere == 1) {
                                    strcpy(msg, "Table content has been printed.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else {
                                    strcpy(msg, "Failed to print columns.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                            }
                        }
                       else {
                            strcpy(msg, "Table not found.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                       }
                    }
                }
            }
            else {
                
                char columns[100][100];
                int num_columns = 0;

                while (token != NULL)
                {
                    // Check if the token is a column name
                    if (strcmp(token, "FROM") != 0)
                    {
                        // Remove commas from the token
                        char* commaPos = strchr(token, ',');
                        if (commaPos != NULL)
                            *commaPos = '\0';

                        strcpy(columns[num_columns], token);
                        num_columns++;
                    }
                    else {
                        break;
                    }

                    token = strtok(NULL, " ");
                }

                for (int i = 0; i < num_columns; i++) {
                    printf("%s\n", columns[i]);
                }

                if(strcmp(token, "FROM") == 0) {
                    token = strtok(NULL, " ");
                    if (token == NULL) 
                    {
                        strcpy(msg, "Unable to perform select.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                    else {
                        char table_path[200];
                        strcpy(table_path, rpath);
                        strcat(table_path, "/");
                        strcat(table_path, use_database);
                        strcat(table_path, "/");
                        strcat(table_path, token);

                        int check_table = find_table(table_path);

                        if(check_table == 51) {
                            token = strtok(NULL, " ");
                            if (token == NULL) 
                            {
                                // print_all(table_path);
                                int select = select_columns(table_path, columns, num_columns);
                                if(select == 1) {
                                    strcpy(msg, "Table content has been printed.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else {
                                    strcpy(msg, "Failed to print columns.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                            }
                            else if(strcmp(token, "WHERE") == 0) {
                                token = strtok(NULL, " ");
                                if (token == NULL) 
                                {
                                    strcpy(msg, "Unable to perform select.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                char col_condition[60];
                                strcpy(col_condition, token);
                                // find condition value
                                token = strtok(NULL, "= ");
                                if (token == NULL) 
                                {
                                    strcpy(msg, "Invalid format.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                printf("%s %s %s\n", table_path, col_condition, token);
                                int selectcolwhere = select_columns_with_condition(table_path, columns, num_columns, col_condition, token);
                                if(selectcolwhere == 1) {
                                    strcpy(msg, "Table content has been printed.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else {
                                    strcpy(msg, "Failed to print columns.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                            }
                            
                        }
                       else {
                            strcpy(msg, "Table not found.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                       }
                    }
                }
            }
        }
        else if (strcmp(token, "DELETE") == 0) {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to perform select.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "FROM") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to perform select.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else {
                    char table_path[200];
                    strcpy(table_path, rpath);
                    strcat(table_path, "/");
                    strcat(table_path, use_database);
                    strcat(table_path, "/");
                    strcat(table_path, token);

                    int check_table = find_table(table_path);

                    if(check_table == 51) {
                        token = strtok(NULL, " ");
                        if (token == NULL) {
                            if(delete_table(table_path) == 1) {
                                strcpy(msg, "Table content has been deleted.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            else {
                                strcpy(msg, "Deleting table content failed.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                        } 
                        else if(strcmp(token, "WHERE") == 0) {
                            token = strtok(NULL, " ");
                            if (token == NULL) 
                            {
                                strcpy(msg, "Unable to perform delete.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            char col_condition[60];
                            strcpy(col_condition, token);
                            // find condition value
                            token = strtok(NULL, "= ");
                            if (token == NULL) 
                            {
                                strcpy(msg, "Invalid format.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            printf("%s %s %s\n", table_path, col_condition, token);
                            int deleteallwhere = delete_table_with_condition(table_path, col_condition, token);
                            if(deleteallwhere == 1) {
                                strcpy(msg, "Table content has been deleted.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            else {
                                strcpy(msg, "Failed to delete table.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                        }
                        
                    }
                }
            }
        }
        else {
            strcpy(msg, "Invalid command.");
            printf("%s\n", msg);
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        
        memset(buffer, 0, 1024);
    }
}
